# Retro Data Collection

Collection of boxart and gamelist data used in RetroPie. The goal of this repo is to have a clean collection of games in (USA) with images and descriptions attached.

No games are included in this repo but the data is based on the "No-Intro-Collection".

## Getting started

1. Download thumbnail packs from http://thumbnailpacks.libretro.com
1. Place images for each system from `<system>/Named_Boxarts/*` into `RetroPie/roms/<system>/boxart/`
1. Copy the gamelists from this repo into `RetroPie/roms/<systems>/`
1. Reboot.

## To-do

- megadrive
- snes
- n64
- gb
- gba
- gbc
- gamegear

## Games to delete

- (Australia)
- (France)
- (Brazil)
- (Europe)
- (Hong Kong)
- (Asia)
- (Japan)
- (Netherlands)
- (China)
- (Korea)
- (Beta)
- (Canada)
- (Proto)
- (Sample)
- Cartridge
- (Demo)
- (Pirate)
